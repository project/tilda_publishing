<?php

/**
 * @file
 * tilda_publishing.module
 */

define('TILDA_CONTENT_TYPE', 'tilda');
define('TILDA_PUBLIC_KEY_VARIABLE', 'tilda_public_key');
define('TILDA_SECRET_KEY_VARIABLE', 'tilda_secret_key');
define('TILDA_REQUEST_TIMEOUT_VARIABLE', 'tilda_request_timeout');
define('TILDA_REQUEST_DEFAULT_TIMEOUT', 60);
define('TILDA_STATUS_OK', 'FOUND');
define('TILDA_SYNC_RESULT', 'ok');
define('TILDA_EMPTY_OPTION', '_none');
define('TILDA_FILE_DIR', 'public://tilda/');
define('TILDA_QUEUE_ID', 'tilda_import_pages');

/**
 * Implements hook_menu().
 */
function tilda_publishing_menu() {
  $items = [];

  $items['admin/config/tilda-settings'] = [
    'title' => 'Tilda settings',
    'description' => 'Custom configuration for Tilda.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['tilda_publishing_settings_form'],
    'access callback' => 'user_access',
    'access arguments' => ['administer tilda configuration'],
  ];

  $items['api/v1/tilda/update'] = [
    'title' => 'Tilda page update callback',
    'page callback' => 'tilda_publishing_api_refresh_callback',
    'delivery callback' => 'tilda_publishing_api_delivery',
    'access callback' => true,
  ];

  return $items;
}

/**
 * Implements hook_permission().
 */
function tilda_publishing_permission() {
  return [
    'administer tilda configuration' => [
      'title' => t('Administer Tilda configuration'),
      'description' => t('Administer Tilda configuration.'),
    ],
  ];
}

/**
 * Implements hook_node_info().
 */
function tilda_publishing_node_info() {
  return [
    TILDA_CONTENT_TYPE => [
      'name' => t('Tilda'),
      'base' => TILDA_CONTENT_TYPE,
      'description' => t('Tilda publising page'),
      'has_title' => true,
      'title_label' => t('Tilda page title'),
    ],
  ];
}

/**
 * Implement hook_form().
 */
function tilda_form($node, $form_state) {
  return node_content_form($node, $form_state);
}

/**
 * General settings form.
 */
function tilda_publishing_settings_form($form, &$form_state) {
  $form[TILDA_PUBLIC_KEY_VARIABLE] = [
    '#type' => 'textfield',
    '#title' => t('Tilda Public key'),
    '#default_value' => variable_get(TILDA_PUBLIC_KEY_VARIABLE, ''),
  ];
  $form[TILDA_SECRET_KEY_VARIABLE] = [
    '#type' => 'textfield',
    '#title' => t('Tilda Secret key'),
    '#default_value' => variable_get(TILDA_SECRET_KEY_VARIABLE, ''),
  ];

  return system_settings_form($form);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter.
 */
function tilda_publishing_form_tilda_node_form_alter(&$form, &$form_state) {
  // Check API keys.
  $public_key = variable_get(TILDA_PUBLIC_KEY_VARIABLE);
  $secret_key = variable_get(TILDA_SECRET_KEY_VARIABLE);
  if (empty($public_key) || empty($secret_key)) {
    drupal_set_message(t('Set API keys for Tilda'), 'warning');
    return;
  }

  drupal_add_library('system', 'drupal.ajax');
  $projects = [
    TILDA_EMPTY_OPTION => t('-- Select Tilda project --'),
  ];

  $request = drupal_http_request('https://api.tildacdn.info/v1/getprojectslist/?publickey=' . $public_key . '&secretkey=' . $secret_key, array('timeout' => variable_get(TILDA_REQUEST_TIMEOUT_VARIABLE, TILDA_REQUEST_DEFAULT_TIMEOUT)));

  $request = drupal_json_decode($request->data);
  if ($request['status'] == TILDA_STATUS_OK) {
    foreach ($request['result'] as $result) {
      $projects[$result['id']] = $result['title'];
    }
  }
  else {
    drupal_set_message(t('Error on Tilda API'), 'warning');
  }

  $form['field_tilda_project_id'][LANGUAGE_NONE]['#options'] = $projects;
  if (!empty($form['#node']) && !empty($form['#node']->field_tilda_project_id[LANGUAGE_NONE][0]['value'])) {
    $form['field_tilda_project_id'][LANGUAGE_NONE]['#default_value'] = $form['#node']->field_tilda_project_id[LANGUAGE_NONE][0]['value'];
  }
  $form['field_tilda_project_id'][LANGUAGE_NONE]['#ajax'] = [
    'event' => 'change',
    'method' => 'replace',
    'callback' => 'tilda_get_allowed_pages_ajax',
    'wrapper' => 'dropdown_pages',
  ];

  $pages = tilda_get_allowed_pages($form, $form_state);

  $form['field_tilda_page_id'][LANGUAGE_NONE]['#options'] = $pages;
  if (!empty($form['#node']) && !empty($form['#node']->field_tilda_page_id[LANGUAGE_NONE][0]['value'])) {
    $form['field_tilda_page_id'][LANGUAGE_NONE]['#default_value'] = $form['#node']->field_tilda_page_id[LANGUAGE_NONE][0]['value'];
  }
  $form['field_tilda_page_id'][LANGUAGE_NONE]['#prefix'] = '<div id="dropdown_pages">';
  $form['field_tilda_page_id'][LANGUAGE_NONE]['#suffix'] = '</div>';
  $form['field_tilda_page_id'][LANGUAGE_NONE]['#ajax'] = [
    'callback' => 'tilda_publishing_set_body_by_page',
    'wrapper' => 'tilda-html-field',
    'event' => 'change',
    'method' => 'replace',
  ];

  $form['field_tilda_page_html'][LANGUAGE_NONE][0]['#format'] = 'tilda_publishing';
  $form['field_tilda_page_html']['#prefix'] = '<div id="tilda-html-field">';
  $form['field_tilda_page_html']['#suffix'] = '</div>';

  // Add a refresh button to re-import data from the Tilda.
  $form['update_body'] = [
    '#type' => 'button',
    '#value' => t('Update'),
    '#executes_submit_callback' => FALSE,
    '#ajax' => [
      'callback' => 'tilda_publishing_set_body_by_page',
      'wrapper' => 'tilda-html-field',
      'event' => 'click',
      'method' => 'replace',
    ],
  ];
}

/**
 * Ajax callback to get allowed pages by project.
 */
function tilda_get_allowed_pages_ajax($form, &$form_state) {
  $values = $form_state['values'];
  $pages[TILDA_EMPTY_OPTION] = t('-- Select the page --');

  if (!empty($values['field_tilda_project_id'][LANGUAGE_NONE][0]['value'])) {
    $value = $values['field_tilda_project_id'][LANGUAGE_NONE][0]['value'];

    $public_key = variable_get(TILDA_PUBLIC_KEY_VARIABLE);
    $secret_key = variable_get(TILDA_SECRET_KEY_VARIABLE);
    $request = drupal_http_request("http://api.tildacdn.info/v1/getpageslist/?publickey=$public_key&secretkey=$secret_key&projectid=$value", array('timeout' => variable_get(TILDA_REQUEST_TIMEOUT_VARIABLE, TILDA_REQUEST_DEFAULT_TIMEOUT)));

    $request = drupal_json_decode($request->data);
    if ($request['status'] == TILDA_STATUS_OK) {
      $pages[TILDA_EMPTY_OPTION] = t('-- Select the page --');
      foreach ($request['result'] as $result) {
        $pages[$result['id']] = $result['title'];
      }
    }
  }

  $form['field_tilda_page_id'][LANGUAGE_NONE]['#options'] = $pages;

  return $form['field_tilda_page_id'][LANGUAGE_NONE];
}

/**
 * Function to get allowed pages by project.
 */
function tilda_get_allowed_pages($form, &$form_state) {
  $pages[TILDA_EMPTY_OPTION] = t('-- Select the page --');

  // After submit, get values from the form.
  if (!empty($form_state['values']['field_tilda_project_id'][LANGUAGE_NONE][0]['value'])) {
    $project_id = $form_state['values']['field_tilda_project_id'][LANGUAGE_NONE][0]['value'];
  }
  // On first form building get values from node.
  elseif (!empty($form['field_tilda_project_id'][LANGUAGE_NONE]['#default_value'])) {
    $project_id = $form['field_tilda_project_id'][LANGUAGE_NONE]['#default_value'];
  }

  if (!empty($project_id)) {
    $public_key = variable_get(TILDA_PUBLIC_KEY_VARIABLE);
    $secret_key = variable_get(TILDA_SECRET_KEY_VARIABLE);
    $request = drupal_http_request("http://api.tildacdn.info/v1/getpageslist/?publickey=$public_key&secretkey=$secret_key&projectid=$project_id", array('timeout' => variable_get(TILDA_REQUEST_TIMEOUT_VARIABLE, TILDA_REQUEST_DEFAULT_TIMEOUT)));

    $request = drupal_json_decode($request->data);
    if ($request['status'] == TILDA_STATUS_OK) {
      foreach ($request['result'] as $result) {
        $pages[$result['id']] = $result['title'];
      }
    }
  }

  return $pages;
}

/**
 * Ajax callback to get body value by page id.
 */
function tilda_publishing_set_body_by_page($form, &$form_state) {
  $values = $form_state['values'];

  if (!empty($values['field_tilda_page_id'][LANGUAGE_NONE][0]['value']) && $values['field_tilda_page_id'][LANGUAGE_NONE][0]['value'] != '_none') {
    $result_body = _tilda_publishing_import_page_content($values['field_tilda_page_id'][LANGUAGE_NONE][0]['value']);
    drupal_alter('tilda_page_html', $result_body);
  }

  $form['field_tilda_page_html'][LANGUAGE_NONE][0]['value']['#value'] = '';
  if (!empty($result_body)) {
    $form['field_tilda_page_html'][LANGUAGE_NONE][0]['value']['#value'] .= $result_body . ' ';
  }

  return $form['field_tilda_page_html'];
}

/**
 * Import page content from Tilda by page_id.
 * Import pictures, CSS and JS.
 *
 * @param int $page_id
 *   Page ID on Tilda.
 *
 * @return string
 *   Page content with CSS and JS.
 */
function _tilda_publishing_import_page_content($page_id) {
  $result_body = '';
  $public_key = variable_get(TILDA_PUBLIC_KEY_VARIABLE);
  $secret_key = variable_get(TILDA_SECRET_KEY_VARIABLE);
  $request = drupal_http_request("http://api.tildacdn.info/v1/getpageexport/?publickey=$public_key&secretkey=$secret_key&pageid=$page_id", array('timeout' => variable_get(TILDA_REQUEST_TIMEOUT_VARIABLE, TILDA_REQUEST_DEFAULT_TIMEOUT)));

  $request = drupal_json_decode($request->data);
  if ($request['status'] == TILDA_STATUS_OK) {
    if ($request['result']['html']) {
      $lp_body = $request['result']['html'];
    }
    else {
      $lp_body = '';
    }

    // Drop a directory with old files for the node.
    $source_dir = TILDA_FILE_DIR . 'page_' . $page_id;
    $real_dir = drupal_realpath($source_dir);
    tilda_publishing_remove_directory($real_dir);

    // Load pictures.
    $dir = $source_dir . '/pictures';
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $image_replaces = [];
    foreach ($request['result']['images'] as $image) {
      if (is_dir(drupal_realpath($dir))) {
        // Prevent URIs with triple slashes when glueing parts together.
        $path = str_replace('///', '//', "$dir/") . $image['to'];
      }
      else {
        $path = $dir;
      }
      $file = file_get_contents($image['from']);
      if ($file) {
        file_put_contents($path, $file);
      }

      $file_url = file_create_url($dir);

      // Sometimes Tilda send same image many times. Avoid multiple replaces.
      $image_replaces[$image['to']] = $file_url . '/' . $image['to'];
    }

    foreach ($image_replaces as $search => $replace) {
      $lp_body = str_replace($search, $replace, $lp_body);
    }

    // Load css.
    $dir = $source_dir . '/css';
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    foreach ($request['result']['css'] as $id => $css) {
      $url = $css['from'];
      $parsed_url = parse_url($url);
      $destination = $dir;
      if (!isset($destination)) {
        $path = file_build_uri(drupal_basename($parsed_url['path']));
      }
      else if (is_dir(drupal_realpath($destination))) {
        // Prevent URIs with triple slashes when glueing parts together.
        $path = str_replace('///', '//', "$destination/") . $css['to'];
      }
      else {
        $path = $destination;
      }

      $result = drupal_http_request($url, array('timeout' => variable_get(TILDA_REQUEST_TIMEOUT_VARIABLE, TILDA_REQUEST_DEFAULT_TIMEOUT)));
      if ($result->code != 200 && $result->code != 0) {
        drupal_set_message(t('HTTP error @errorcode occurred when trying to fetch @remote.', [
          '@errorcode' => $result->code,
          '@remote' => $url,
        ]), 'error');
      }
      $local = file_save_data($result->data, $path, TRUE);
      if (!$local) {
        drupal_set_message(t('@remote could not be saved to @path.', [
          '@remote' => $url,
          '@path' => $path,
        ]), 'error');
      }

      // Add CSS to result.
      $result_body .= ' <link rel="stylesheet" type="text/css" href="' . file_create_url($local->uri) . '">' . PHP_EOL;
    }

    // Load js.
    $dir = $source_dir . '/js';
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    foreach ($request['result']['js'] as $id => $js) {
      $url = $js['from'];
      $parsed_url = parse_url($url);
      $destination = $dir;
      if (!isset($destination)) {
        $path = file_build_uri(drupal_basename($parsed_url['path']));
      }
      else if (is_dir(drupal_realpath($destination))) {
        // Prevent URIs with triple slashes when glueing parts together.
        $path = str_replace('///', '//', "$destination/") . $js['to'];
      }
      else {
        $path = $destination;
      }

      $result = drupal_http_request($url, array('timeout' => variable_get(TILDA_REQUEST_TIMEOUT_VARIABLE, TILDA_REQUEST_DEFAULT_TIMEOUT)));
      if ($result->code != 200 && $result->code != 0) {
        drupal_set_message(t('HTTP error @errorcode occurred when trying to fetch @remote.', [
          '@errorcode' => $result->code,
          '@remote' => $url,
        ]), 'error');
      }
      $local = file_save_data($result->data, $path, TRUE);
      if (!$local) {
        drupal_set_message(t('@remote could not be saved to @path.', [
          '@remote' => $url,
          '@path' => $path,
        ]), 'error');
      }

      // Add JS to result.
      $result_body .= ' <script src="' . file_create_url($local->uri) . '" type="text/javascript"></script>' . PHP_EOL;
    }

    // Fill body.
    $result_body .= $lp_body . ' ';
  }

  return $result_body;
}

/**
 * Recursively delete a directory.
 *
 * @param string $real_path
 *   Real path.
 */
function tilda_publishing_remove_directory($real_path) {
  if (is_dir($real_path)) {
    $objects = scandir($real_path, 0);
    foreach ($objects as $object) {
      if ($object != '.' && $object != '..') {
        $inside_item = $real_path . '/' . $object;
        if (!is_dir($inside_item)) {
          unlink($inside_item);
        }
        else {
          tilda_publishing_remove_directory($inside_item);
        }
      }
    }
    rmdir($real_path);
  }
}

/**
 * Implements of hook_preprocess_page().
 */
function tilda_publishing_preprocess_page(&$variables) {
  if (isset($variables['node'], $variables['node']->type) && $variables['node']->type === TILDA_CONTENT_TYPE) {
    $node = $variables['node'];
    $node_path = 'node/' . $node->nid;
    $alias = drupal_get_path_alias($node_path);
    if (in_array(current_path(), [$node_path, $alias])) {
      $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
    }
  }
}

/**
 * Implements hook_theme_registry_alter().
 */
function tilda_publishing_theme_registry_alter(&$theme_registry) {
  // Defined path to the current module.
  $module_path = drupal_get_path('module', 'tilda_publishing');
  // Find all .tpl.php files in this module's folder recursively.
  $template_file_objects = drupal_find_theme_templates($theme_registry, '.tpl.php', $module_path);
  // Iterate through all found template file objects.
  foreach ($template_file_objects as $key => $template_file_object) {
    // If the template has not already been overridden by a theme.
    if (!isset($theme_registry[$key]['theme path']) || !preg_match('#/themes/#', $theme_registry[$key]['theme path'])) {
      // Alter the theme path and template elements.
      $theme_registry[$key]['theme path'] = $module_path;
      $theme_registry[$key] = array_merge($theme_registry[$key], $template_file_object);
      $theme_registry[$key]['type'] = 'module';
    }
  }
}

/**
 * Implements of hook_preprocess_html().
 */
function tilda_publishing_preprocess_html(&$vars) {
  if ($node = menu_get_object()) {
    $node_path = 'node/' . $node->nid;
    $alias = drupal_get_path_alias($node_path);
    if ($node->type === TILDA_CONTENT_TYPE && in_array(current_path(), array(
        $node_path,
        $alias,
      ))) {
      $vars['theme_hook_suggestions'][] = 'html__tilda__fullscreen';
    }
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function tilda_publishing_cron_queue_info() {
  $queues = [];
  $queues[TILDA_QUEUE_ID] = [
    'worker callback' => 'tilda_publishing_update_page',
    'time' => 60,
  ];
  return $queues;
}

/**
 * Set a ticket to refresh some page from Tilda.
 */
function tilda_publishing_api_refresh_callback() {
  // Check input arguments.
  if (empty($_GET['pageid']) || empty($_GET['projectid']) || empty($_GET['publickey'])) {
    watchdog('tilda', 'API request without all arguments: GET - %params', ['%params' => http_build_query($_GET)]);
    return TILDA_SYNC_RESULT;
  }

  // Compare public key.
  $public_key = variable_get(TILDA_PUBLIC_KEY_VARIABLE);
  if (!$public_key || $public_key != $_GET['publickey']) {
    watchdog('tilda', 'API request with incorrect publickey: GET - %params', ['%params' => http_build_query($_GET)]);
    return TILDA_SYNC_RESULT;
  }

  // Get nids by project and page.
  $query = db_select('node', 'node');
  $query->fields('node', ['nid']);
  $query->join('field_data_field_tilda_project_id', 'project', 'project.entity_id = node.nid AND project.entity_type = \'node\'');
  $query->join('field_data_field_tilda_page_id', 'page', 'page.entity_id = node.nid AND page.entity_type = \'node\'');
  $query->join('field_data_field_tilda_autoupdate', 'autoupdate', 'autoupdate.entity_id = node.nid AND autoupdate.entity_type = \'node\'');
  $query->condition('project.field_tilda_project_id_value', $_GET['projectid']);
  $query->condition('page.field_tilda_page_id_value', $_GET['pageid']);
  $query->condition('autoupdate.field_tilda_autoupdate_value', 1);
  $result = $query->execute();
  foreach ($result as $item) {
    $my_queue = DrupalQueue::get(TILDA_QUEUE_ID);
    $my_queue->createItem($item->nid);
  }

  return TILDA_SYNC_RESULT;
}

/**
 * Empty delivery callback for Tilda API.
 *
 * @param string $var
 *
 * @return string
 */
function tilda_publishing_api_delivery($var) {
  echo $var;
}

function tilda_publishing_update_page($nid) {
  $node = node_load($nid);
  if ($node && !empty($node->field_tilda_page_id[LANGUAGE_NONE][0]['value'])) {
    $page_id = $node->field_tilda_page_id[LANGUAGE_NONE][0]['value'];
    $body = _tilda_publishing_import_page_content($page_id);
    $node->field_tilda_page_html[LANGUAGE_NONE][0]['value'] = $body;
    node_save($node);
  }
}
